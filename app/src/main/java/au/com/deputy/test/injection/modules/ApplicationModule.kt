package au.com.deputy.test.injection.modules

import android.app.Application
import android.content.Context
import au.com.deputy.test.injection.annotations.AppContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    @AppContext
    fun provideContext(): Context = application
}