package au.com.deputy.test.data.managers

import au.com.deputy.test.data.api.CallCoordinator
import au.com.deputy.test.data.api.models.ShiftStatusChangeRequest
import au.com.deputy.test.data.api.services.ShiftService
import au.com.deputy.test.data.mappers.ShiftListingMapper
import au.com.deputy.test.domain.models.entities.Shift
import au.com.deputy.test.domain.repositories.ShiftRepository
import au.com.deputy.test.util.toApiDateString
import io.reactivex.Completable
import io.reactivex.Single
import java.util.Date
import javax.inject.Inject

class ShiftManager @Inject constructor(private val shiftService: ShiftService,
                                       private val callCoordinator: CallCoordinator) : ShiftRepository {
    override fun getShifts(): Single<List<Shift>> {
        return Single.fromCallable {
            val response = callCoordinator.execute(shiftService.getShifts())
            ShiftListingMapper.mapToDomainObject(response)
        }
    }

    override fun startShift(latitude: Double, longitude: Double, time: Date): Completable {
        return Completable.fromCallable {
            val requestBody = ShiftStatusChangeRequest(
                time.toApiDateString(),
                latitude.toString(),
                longitude.toString()
            )
            callCoordinator.execute(shiftService.startShift(requestBody))
        }
    }

    override fun endShift(latitude: Double, longitude: Double, time: Date): Completable {
        return Completable.fromCallable {
            val requestBody = ShiftStatusChangeRequest(
                time.toApiDateString(),
                latitude.toString(),
                longitude.toString()
            )
            callCoordinator.execute(shiftService.endShift(requestBody))
        }
    }
}