package au.com.deputy.test.injection.components

import android.content.Context
import au.com.deputy.test.domain.repositories.ShiftRepository
import au.com.deputy.test.injection.annotations.AppContext

interface Singletons {
    @AppContext
    fun provideContext(): Context

    fun provideShiftRepository(): ShiftRepository
}