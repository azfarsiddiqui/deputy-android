package au.com.deputy.test.domain.repositories

import au.com.deputy.test.domain.models.entities.Shift
import io.reactivex.Completable
import io.reactivex.Single
import java.util.Date

interface ShiftRepository {
    fun getShifts(): Single<List<Shift>>
    fun startShift(latitude: Double, longitude: Double, time: Date): Completable
    fun endShift(latitude: Double, longitude: Double, time: Date): Completable
}