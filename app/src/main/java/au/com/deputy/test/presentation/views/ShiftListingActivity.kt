package au.com.deputy.test.presentation.view

import android.Manifest
import android.Manifest.permission
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build.VERSION_CODES
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import au.com.deputy.test.R
import au.com.deputy.test.domain.models.entities.Shift
import au.com.deputy.test.injection.modules.ActivityModule
import au.com.deputy.test.presentation.ShiftListAdapter
import au.com.deputy.test.presentation.presenters.ShiftListingPresenter
import au.com.deputy.test.presentation.presenters.ShiftListingPresenter.ViewSurface
import kotlinx.android.synthetic.main.activity_shift_listing.*
import javax.inject.Inject


class ShiftListingActivity : BaseActivity(), ViewSurface {

    companion object {
        const val PERMISSIONS_REQUEST_ACCESS_LOCATION = 18
        const val KEY_BUNDLE_SHIFTS = "SHIFTS"
        const val KEY_BUNDLE_START_BUTTON_STATUS = "START_BUTTON_STATUS"
        const val KEY_BUNDLE_END_BUTTON_STATUS = "END_BUTTON_STATUS"
    }

    @Inject
    lateinit var presenter: ShiftListingPresenter

    var locationManager: LocationManager? = null

    private lateinit var shifts: ArrayList<Shift>

    private var isStartShiftOperationRequested = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectActivity()
        setContentView(R.layout.activity_shift_listing)
        hookupListeners()

        presenter.onCreate(this)

        if (savedInstanceState != null
            && savedInstanceState.containsKey(KEY_BUNDLE_SHIFTS)) {
            restoreStateFromBundle(savedInstanceState)
        }
        else {
            presenter.loadShifts()
        }
    }

    private fun hookupListeners() {
        btn_start_shift.setOnClickListener {
            isStartShiftOperationRequested = true
            this@ShiftListingActivity.checkForPermissionOrStartLocationUpdates()
        }

        btn_end_shift.setOnClickListener {
            isStartShiftOperationRequested = false
            this@ShiftListingActivity.checkForPermissionOrStartLocationUpdates()
        }
    }

    override fun showShifts(shifts: List<Shift>) {
        this.shifts = ArrayList(shifts)
        recycler_view.layoutManager = LinearLayoutManager(this)
        val itemDecor = DividerItemDecoration(this, HORIZONTAL)
        recycler_view.addItemDecoration(itemDecor)
        recycler_view.adapter = ShiftListAdapter(shifts)
    }

    override fun showError(message: String?) {
        message?.let {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun setStartShiftButtonEnabled(enabled: Boolean) {
        btn_start_shift.isEnabled = enabled
    }

    override fun setEndShiftButtonEnabled(enabled: Boolean) {
        btn_end_shift.isEnabled = enabled
    }

    override fun showLoader() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        progress_bar.visibility = View.GONE
    }

    private fun restoreStateFromBundle(bundle: Bundle) {
        shifts = bundle.getParcelableArrayList(KEY_BUNDLE_SHIFTS)
        btn_start_shift.isEnabled = bundle.getBoolean(KEY_BUNDLE_START_BUTTON_STATUS)
        btn_end_shift.isEnabled = bundle.getBoolean(KEY_BUNDLE_END_BUTTON_STATUS)

        showShifts(shifts)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(KEY_BUNDLE_SHIFTS, shifts)
        outState.putBoolean(KEY_BUNDLE_START_BUTTON_STATUS, btn_start_shift.isEnabled)
        outState.putBoolean(KEY_BUNDLE_END_BUTTON_STATUS, btn_end_shift.isEnabled)
    }

    override fun onStop() {
        super.onStop()
        locationManager?.removeUpdates(locationListener)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        when {
            locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER) ->
                locationManager?.requestSingleUpdate(LocationManager.GPS_PROVIDER, locationListener, null)
            locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ->
                locationManager?.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locationListener, null)
            else -> Toast.makeText(this, R.string.error_fetching_location, Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkForPermissionOrStartLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            startLocationUpdates()
        }
        else {
            requestLocationPermission()
        }
    }

    @TargetApi(VERSION_CODES.M)
    private fun requestLocationPermission() {
        requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION),
            PERMISSIONS_REQUEST_ACCESS_LOCATION
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_LOCATION
            && grantResults.isNotEmpty()
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            startLocationUpdates()
        }
    }

    private val locationListener = object : LocationListener {

        override fun onLocationChanged(location: Location) {
            locationManager?.removeUpdates(this)

            if (isStartShiftOperationRequested) presenter.startShift(location.latitude, location.longitude)
            else  presenter.endShift(location.latitude, location.longitude)
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        }

        override fun onProviderEnabled(provider: String) {
        }

        override fun onProviderDisabled(provider: String) {
        }
    }

    override fun injectActivity() = getAppInjector().activityModule(ActivityModule(this)).build().inject(this)
}
