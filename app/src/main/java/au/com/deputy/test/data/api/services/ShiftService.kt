package au.com.deputy.test.data.api.services

import au.com.deputy.test.data.api.models.Shift
import au.com.deputy.test.data.api.models.ShiftStatusChangeRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface ShiftService {
    @GET("shifts")
    fun getShifts(): Call<List<Shift>>

    @POST("shift/start")
    fun startShift(@Body requestBody: ShiftStatusChangeRequest): Call<Void>

    @POST("shift/end")
    fun endShift(@Body requestBody: ShiftStatusChangeRequest): Call<Void>
}