package au.com.deputy.test

import android.app.Application
import au.com.deputy.test.injection.components.ApplicationComponent
import au.com.deputy.test.injection.components.DaggerApplicationComponent
import au.com.deputy.test.injection.modules.ApplicationModule
import javax.inject.Inject

class DeputyApplication : Application() {

    @Inject lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        inject()
    }

    fun inject(){
        DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build().inject(this)
    }
}