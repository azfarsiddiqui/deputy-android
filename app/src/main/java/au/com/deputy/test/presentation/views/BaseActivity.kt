package au.com.deputy.test.presentation.view

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import au.com.deputy.test.DeputyApplication
import au.com.deputy.test.injection.components.DaggerActivityComponent

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        injectActivity()
    }

    private fun getAppComponent() = (applicationContext as DeputyApplication).applicationComponent

    fun getAppInjector() = DaggerActivityComponent.builder().applicationComponent(getAppComponent())

    abstract fun injectActivity()
}