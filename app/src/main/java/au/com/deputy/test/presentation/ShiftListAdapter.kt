package au.com.deputy.test.presentation

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import au.com.deputy.test.R
import au.com.deputy.test.domain.models.entities.Shift
import au.com.deputy.test.domain.models.entities.ShiftStatus.ENDED
import au.com.deputy.test.domain.models.entities.ShiftStatus.STARTED
import au.com.deputy.test.util.toUIString
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.list_item_shift.view.*

class ShiftListAdapter(val shifts: List<Shift>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount() = shifts.size

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        val view = layoutInflater.inflate(R.layout.list_item_shift, parent, false)
        return ShiftItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ShiftItemViewHolder).bindData(shifts[position])
    }

    class ShiftItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(shift: Shift) {
            with(itemView) {
                Glide.with(context).load(shift.imageUrl).into(img_thumb)
                txt_start_time.text = context.getString(R.string.shift_started_at, shift.startTime.toUIString())
                txt_end_time.text = when (shift.status) {
                    ENDED -> context.getString(R.string.shift_ended_at, shift.endTime?.toUIString())
                    STARTED -> context.getString(R.string.shift_not_ended_yet)
                }
            }
        }
    }
}

