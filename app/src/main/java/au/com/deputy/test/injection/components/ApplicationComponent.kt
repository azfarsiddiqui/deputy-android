package au.com.deputy.test.injection.components

import au.com.deputy.test.DeputyApplication
import au.com.deputy.test.injection.modules.ApplicationModule
import au.com.deputy.test.injection.modules.NetworkModule
import au.com.deputy.test.injection.modules.SingletonModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (SingletonModule::class), (NetworkModule::class)])
interface ApplicationComponent: Singletons {

    fun inject(application: DeputyApplication)
}