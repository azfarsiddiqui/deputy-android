package au.com.deputy.test.presentation.presenters

import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter {

    val compositeDisposable = CompositeDisposable()

    fun onDestroy() {
        compositeDisposable.clear()
    }
}