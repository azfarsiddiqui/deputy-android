package au.com.deputy.test.data.api

import retrofit2.Call
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CallCoordinator @Inject constructor() {
    fun <T> execute(call: Call<T>): T {
        try {
            val response = call.execute()

            if (response.isSuccessful) {
                return response.body()
            } else {
                throw Exception()
            }
        } catch (exception: Exception) {
            throw exception
        }
    }

}

