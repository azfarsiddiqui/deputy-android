package au.com.deputy.test.data.api.models

import com.google.gson.annotations.SerializedName

data class ShiftStatusChangeRequest(
    @SerializedName("time") val time: String,
    @SerializedName("latitude") val latitude: String,
    @SerializedName("longitude") val longitude: String
)