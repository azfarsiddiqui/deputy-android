package au.com.deputy.test.data.mappers

import au.com.deputy.test.data.api.models.Shift
import au.com.deputy.test.domain.models.entities.ShiftStatus.ENDED
import au.com.deputy.test.domain.models.entities.ShiftStatus.STARTED
import au.com.deputy.test.util.parseDDMMYYYY
import au.com.deputy.test.domain.models.entities.Shift as ShiftModel

class ShiftListingMapper {
    companion object {
        fun mapToDomainObject(response: List<Shift>?): List<ShiftModel> {
            val shifts = mutableListOf<ShiftModel>()

            response?.forEach { shift ->
                val startDateTime = shift.startTime.parseDDMMYYYY()
                val endDateTime = shift.endTime.isNotEmpty().let {
                    if (it) shift.endTime.parseDDMMYYYY()
                    else null
                }
                val shift = ShiftModel(
                    id = shift.id,
                    startTime = startDateTime,
                    endTime = endDateTime,
                    status = if (endDateTime == null) STARTED else ENDED,
                    startLatitude = shift.startLatitude.toDouble(),
                    startLongitude = shift.startLongitude.toDouble(),
                    endLatitude = shift.endLatitude?.toDouble(),
                    endLongitude = shift.endLongitude?.toDouble(),
                    imageUrl = shift.imageUrl
                )

                shifts.add(shift)
            }

            return shifts
        }
    }
}