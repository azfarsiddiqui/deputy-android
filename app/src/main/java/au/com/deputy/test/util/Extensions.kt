package au.com.deputy.test.util

import java.text.SimpleDateFormat
import java.util.Date


fun String.parseDDMMYYYY(): Date {
    val parseFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
    return parseFormatter.parse(this)
}

fun Date.toApiDateString(): String {
    val outputFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
    return outputFormatter.format(this)
}

fun Date.toUIString(): String {
    val outputFormatter = SimpleDateFormat("HH:mm 'on' dd-MM")
    return outputFormatter.format(this)
}