package au.com.deputy.test.injection.modules

import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import au.com.deputy.test.injection.annotations.ActivityContext
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    fun provideAppCompatActivity(): AppCompatActivity = activity

    @Provides
    fun provideActivity(): Activity = activity

    @Provides
    @ActivityContext
    fun provideContext(): Context = activity

}
