package au.com.deputy.test.injection.modules

import au.com.deputy.test.data.managers.ShiftManager
import au.com.deputy.test.domain.repositories.ShiftRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface SingletonModule {

    @Binds
    @Singleton
    fun bindShiftRepository(shiftManager: ShiftManager): ShiftRepository
}