package au.com.deputy.test.domain.usecases

import au.com.deputy.test.domain.repositories.ShiftRepository
import io.reactivex.schedulers.Schedulers
import java.util.Date
import javax.inject.Inject

class EndShiftUseCase @Inject constructor(private val shiftRepository: ShiftRepository) {

    fun execute(latitude: Double, longitude: Double) =
        shiftRepository.endShift(latitude, longitude, Date()).subscribeOn(Schedulers.io())
}