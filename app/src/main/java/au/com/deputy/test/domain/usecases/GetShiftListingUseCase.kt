package au.com.deputy.test.domain.usecases

import au.com.deputy.test.domain.repositories.ShiftRepository
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetShiftListingUseCase @Inject constructor(private val shiftRepository: ShiftRepository) {

    fun execute() = shiftRepository.getShifts().subscribeOn(Schedulers.io())
}