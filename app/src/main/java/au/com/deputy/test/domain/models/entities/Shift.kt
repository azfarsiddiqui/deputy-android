package au.com.deputy.test.domain.models.entities

import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import java.util.Date

data class Shift(
    val id: Int,
    val startTime: Date,
    val endTime: Date?,
    val startLatitude: Double,
    val startLongitude: Double,
    val endLatitude: Double?,
    val endLongitude: Double?,
    val imageUrl: String,
    val status: ShiftStatus
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readSerializable() as Date,
        parcel.readSerializable() as Date?,
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readValue(Float::class.java.classLoader) as? Double,
        parcel.readValue(Float::class.java.classLoader) as? Double,
        parcel.readString(),
        ShiftStatus.valueOf(parcel.readString())
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeSerializable(startTime)
        parcel.writeSerializable(endTime)
        parcel.writeDouble(startLatitude)
        parcel.writeDouble(startLongitude)
        parcel.writeValue(endLatitude)
        parcel.writeValue(endLongitude)
        parcel.writeString(imageUrl)
        parcel.writeString(status.name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Creator<Shift> {
        override fun createFromParcel(parcel: Parcel): Shift {
            return Shift(parcel)
        }

        override fun newArray(size: Int): Array<Shift?> {
            return arrayOfNulls(size)
        }
    }
}

enum class ShiftStatus {
    STARTED,
    ENDED
}