package au.com.deputy.test.injection.components

import android.content.Context
import android.support.v7.app.AppCompatActivity
import au.com.deputy.test.injection.annotations.ActivityContext
import au.com.deputy.test.injection.annotations.PerScreen
import au.com.deputy.test.injection.modules.ActivityModule
import au.com.deputy.test.presentation.view.ShiftListingActivity

import dagger.Component

@PerScreen
@Component(dependencies = [(ApplicationComponent::class)],
        modules = [(ActivityModule::class)]
)
interface ActivityComponent : Singletons {

    fun provideAppCompatActivity() : AppCompatActivity

    @ActivityContext
    fun provideActivityContext() : Context

    fun inject(activity: ShiftListingActivity)
}