package au.com.deputy.test.presentation.presenters

import au.com.deputy.test.domain.models.entities.Shift
import au.com.deputy.test.domain.models.entities.ShiftStatus.ENDED
import au.com.deputy.test.domain.models.entities.ShiftStatus.STARTED
import au.com.deputy.test.domain.usecases.EndShiftUseCase
import au.com.deputy.test.domain.usecases.GetShiftListingUseCase
import au.com.deputy.test.domain.usecases.StartShiftUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class ShiftListingPresenter @Inject constructor() : BasePresenter() {

    @Inject
    lateinit var getShiftListingUseCase: GetShiftListingUseCase

    @Inject
    lateinit var startShiftUseCase: StartShiftUseCase

    @Inject
    lateinit var endShiftUseCase: EndShiftUseCase

    lateinit var view: ViewSurface

    fun onCreate(viewSurface: ViewSurface) {
        view = viewSurface
    }

    fun loadShifts() {
        compositeDisposable.add(getShiftListingUseCase.execute()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { view.showLoader() }
            .doOnSuccess { view.hideLoader() }
            .doOnError { view.hideLoader() }
            .map { it.reversed() }
            .subscribe({
                if (it.isNotEmpty()) {
                    it.first()?.let {
                        view.setStartShiftButtonEnabled(enabled = it.status == ENDED)
                        view.setEndShiftButtonEnabled(enabled = it.status == STARTED)
                    }
                    view.showShifts(it)
                }
                else {
                    view.setStartShiftButtonEnabled(enabled = true)
                }
            }, {
                showError(it)
            })
        )
    }

    fun startShift(latitude: Double, longitude: Double) {
        compositeDisposable.add(startShiftUseCase.execute(latitude, longitude)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                view.setStartShiftButtonEnabled(enabled = false)
                view.showLoader()
            }
            .doOnComplete {
                view.hideLoader()
                view.setStartShiftButtonEnabled(enabled = false)
                view.setEndShiftButtonEnabled(enabled = true)
            }
            .doOnError {
                view.setStartShiftButtonEnabled(enabled = true)
                view.hideLoader()
                view.showError(it.message)
            }
            .andThen { loadShifts() }
            .subscribe()
        )
    }

    fun endShift(latitude: Double, longitude: Double) {
        compositeDisposable.add(endShiftUseCase.execute(latitude, longitude)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                view.setEndShiftButtonEnabled(enabled = false)
                view.showLoader()
            }
            .doOnComplete {
                view.hideLoader()
                view.setStartShiftButtonEnabled(enabled = true)
                view.setEndShiftButtonEnabled(enabled = false)
            }
            .doOnError {
                view.setEndShiftButtonEnabled(enabled = true)
                view.hideLoader()
                view.showError(it.message)
            }
            .andThen { loadShifts() }
            .subscribe()
        )
    }

    private fun showError(throwable: Throwable) {
        view.showError(throwable.message)
    }

    interface ViewSurface {
        fun showShifts(shifts: List<Shift>)
        fun showError(message: String?)
        fun setStartShiftButtonEnabled(enabled: Boolean)
        fun setEndShiftButtonEnabled(enabled: Boolean)
        fun showLoader()
        fun hideLoader()
    }
}