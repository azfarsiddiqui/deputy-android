package au.com.deputy.test.data.api.interceptors

import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response

class AuthenticatedApiRequestInterceptor(val authToken: String) : Interceptor {
    override fun intercept(chain: Chain): Response {
        val request = chain.request()
        val newRequest = request.newBuilder()

        newRequest.addHeader("Authorization", authToken)

        return chain.proceed(newRequest.build())
    }
}