package au.com.deputy.test.injection.modules

import au.com.deputy.test.Configuration
import au.com.deputy.test.data.api.interceptors.AuthenticatedApiRequestInterceptor
import au.com.deputy.test.data.api.services.ShiftService
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideConfiguration() = Configuration()

    @Singleton
    @Provides
    fun provideRetrofit(configuration: Configuration) = buildRetrofit(configuration)

    private fun buildRetrofit(configuration: Configuration): Retrofit {
        val authInterceptor = AuthenticatedApiRequestInterceptor(configuration.authToken)

        return Retrofit.Builder().baseUrl(configuration.baseUrl)
            .client(OkHttpClient.Builder().addInterceptor(authInterceptor).build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    @Singleton
    @Provides
    fun provideShiftService(retrofit: Retrofit) = retrofit.create(ShiftService::class.java)
}