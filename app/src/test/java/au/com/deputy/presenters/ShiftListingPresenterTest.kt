package au.com.deputy.presenters

import au.com.deputy.test.domain.models.entities.Shift
import au.com.deputy.test.domain.models.entities.ShiftStatus.ENDED
import au.com.deputy.test.domain.models.entities.ShiftStatus.STARTED
import au.com.deputy.test.domain.usecases.EndShiftUseCase
import au.com.deputy.test.domain.usecases.GetShiftListingUseCase
import au.com.deputy.test.domain.usecases.StartShiftUseCase
import au.com.deputy.test.presentation.presenters.ShiftListingPresenter
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.Date

class ShiftListingPresenterTest {

    @Mock
    private
    lateinit var view: ShiftListingPresenter.ViewSurface

    @Mock
    private
    lateinit var getShiftListingUseCase: GetShiftListingUseCase

    @Mock
    private
    lateinit var startShiftUseCase: StartShiftUseCase

    @Mock
    private
    lateinit var endShiftUseCase: EndShiftUseCase

    private val presenter = ShiftListingPresenter()

    @Before
    fun setup() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> Schedulers.trampoline() }

        MockitoAnnotations.initMocks(this)
        presenter.getShiftListingUseCase = getShiftListingUseCase
        presenter.startShiftUseCase = startShiftUseCase
        presenter.endShiftUseCase = endShiftUseCase

        presenter.onCreate(view)
    }

    @Test
    fun loadShifts_exceptionOccured_loaderGetsShownAndHiddenAndErrorMessageIsShown() {
        Mockito.`when`(getShiftListingUseCase.execute()).thenReturn(Single.error(Exception("exception")))

        presenter.loadShifts()

        Mockito.verify(view, Mockito.times(1)).showLoader()
        Mockito.verify(view, Mockito.times(1)).hideLoader()
        Mockito.verify(view, Mockito.times(1)).showError(Mockito.anyString())
    }

    @Test
    fun startShift_exceptionOccured_loaderGetsShownAndHidden() {
        Mockito.`when`(startShiftUseCase.execute(0.0, 0.0)).thenReturn(Completable.error(Exception("exception")))

        presenter.startShift(0.0, 0.0)

        Mockito.verify(view, Mockito.times(1)).showLoader()
        Mockito.verify(view, Mockito.times(1)).hideLoader()
        Mockito.verify(view, Mockito.times(1)).showError(Mockito.anyString())
    }

    @Test
    fun endShift_exceptionOccured_loaderGetsShownAndHidden() {
        Mockito.`when`(endShiftUseCase.execute(0.0, 0.0)).thenReturn(Completable.error(Exception("exception")))

        presenter.endShift(0.0, 0.0)

        Mockito.verify(view, Mockito.times(1)).showLoader()
        Mockito.verify(view, Mockito.times(1)).hideLoader()
        Mockito.verify(view, Mockito.times(1)).showError(Mockito.anyString())
    }

    @Test
    fun loadShifts_gotLatestShiftNotYetComplete_startButtonIsDisabled() {
        val shifts = listOf(Shift(
            id = 1,
            startTime = Date(),
            endTime = null,
            startLongitude = 0.0,
            startLatitude = 0.0,
            endLatitude = 0.0,
            endLongitude = 0.0,
            imageUrl = "",
            status = STARTED
        ))
        Mockito.`when`(getShiftListingUseCase.execute()).thenReturn(Single.just(shifts))

        presenter.loadShifts()

        Mockito.verify(view, Mockito.times(1)).setStartShiftButtonEnabled(false)
        Mockito.verify(view, Mockito.times(1)).setEndShiftButtonEnabled(true)
    }

    @Test
    fun loadShifts_gotLatestShiftWithStatusComplete_startButtonIsEnabledAndEndButtonDisabled() {
        val shifts = listOf(Shift(
            id = 1,
            startTime = Date(),
            endTime = null,
            startLongitude = 0.0,
            startLatitude = 0.0,
            endLatitude = 0.0,
            endLongitude = 0.0,
            imageUrl = "",
            status = ENDED
        ))
        Mockito.`when`(getShiftListingUseCase.execute()).thenReturn(Single.just(shifts))

        presenter.loadShifts()

        Mockito.verify(view, Mockito.times(1)).setStartShiftButtonEnabled(true)
        Mockito.verify(view, Mockito.times(1)).setEndShiftButtonEnabled(false)
    }

    @Test
    fun startShift_completedSuccessfully_buttonStatesAreCorrectAndShiftsAreRefreshed() {
        val shifts = listOf(Shift(
            id = 1,
            startTime = Date(),
            endTime = null,
            startLongitude = 0.0,
            startLatitude = 0.0,
            endLatitude = 0.0,
            endLongitude = 0.0,
            imageUrl = "",
            status = STARTED
        ))
        Mockito.`when`(getShiftListingUseCase.execute()).thenReturn(Single.just(shifts))
        Mockito.`when`(startShiftUseCase.execute(0.0, 0.0)).thenReturn(Completable.complete())

        presenter.startShift(0.0, 0.0)

        Mockito.verify(view, Mockito.times(2)).setEndShiftButtonEnabled(true)
        Mockito.verify(view, Mockito.times(1)).showShifts(shifts)
    }

    @Test
    fun endShift_completedSuccessfully_buttonStatesAreCorrectAndShiftsAreRefreshed() {
        val shifts = listOf(Shift(
            id = 1,
            startTime = Date(),
            endTime = null,
            startLongitude = 0.0,
            startLatitude = 0.0,
            endLatitude = 0.0,
            endLongitude = 0.0,
            imageUrl = "",
            status = STARTED
        ))
        Mockito.`when`(getShiftListingUseCase.execute()).thenReturn(Single.just(shifts))
        Mockito.`when`(endShiftUseCase.execute(0.0, 0.0)).thenReturn(Completable.complete())

        presenter.endShift(0.0, 0.0)

        Mockito.verify(view, Mockito.times(1)).setStartShiftButtonEnabled(true)
        Mockito.verify(view, Mockito.times(1)).showShifts(shifts)
    }
}